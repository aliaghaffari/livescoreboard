package models;

import java.time.LocalDateTime;
import java.util.StringTokenizer;

public class Match {
    String homeTeam;
    String awayTeam;
    int homeTeamScore;
    int awayTeamScore;
    int total_score;
    int status;//0-->start   1--->end
    LocalDateTime startTime;


    public Match() {
    }

    public Match(String homeTeam, String awayTeam) {
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.startTime = LocalDateTime.now();
    }

    public String getMatchTitle() {
        return homeTeam + "-" + awayTeam;
    }

    public Match updateScore(String scores) {
        StringTokenizer st = new StringTokenizer(scores, "-");
        this.homeTeamScore = Integer.valueOf(st.nextToken());
        this.awayTeamScore = Integer.valueOf(st.nextToken());
        this.total_score = homeTeamScore + awayTeamScore;
        return this;
    }

    @Override
    public String toString() {
        return homeTeam + " " + homeTeamScore + " - " + awayTeam + " " + awayTeamScore;
    }


    public String getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(String homeTeam) {
        this.homeTeam = homeTeam;
    }

    public String getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(String awayTeam) {
        this.awayTeam = awayTeam;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public int getHomeTeamScore() {
        return homeTeamScore;
    }

    public void setHomeTeamScore(int homeTeamScore) {
        this.homeTeamScore = homeTeamScore;
    }

    public int getAwayTeamScore() {
        return awayTeamScore;
    }

    public void setAwayTeamScore(int awayTeamScore) {
        this.awayTeamScore = awayTeamScore;
    }

    public int getTotal_score() {
        return total_score;
    }

    public void setTotal_score(int total_score) {
        this.total_score = total_score;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
