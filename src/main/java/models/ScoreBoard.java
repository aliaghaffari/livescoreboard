package models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ScoreBoard {
    private static ScoreBoard self;
    public static List<Match> matches = new ArrayList();

    public static ScoreBoard getInstance() {
        if (self == null) {
            self = new ScoreBoard();
        }
        return self;
    }

    public void deleteAll() {
        matches = new ArrayList();
    }

    public Match startMatch(Match match) {
        matches.add(match);
        return match;
    }

    public Match updateMatchScore(String matchTitle, String scores) {
        Match match = findMatchByMatchTitle(matchTitle);
        if (match != null) {
            match.updateScore(scores);
        } else {
            match = new Match();
        }
        return match;
    }

    public Match findMatchByMatchTitle(String matchTitle) {
        return matches.stream()
                .filter(match -> matchTitle.equals(match.getMatchTitle()))
                .findAny()
                .orElse(null);
    }

    public String summary() {
        removeDoneMatches();
        return initSummary();
    }

    public String initSummary() {
        StringBuilder stringBuilder = new StringBuilder();
        Collections.sort(matches, new MatchCompare());
        List<String> collect = IntStream.range(0, matches.size())
                .mapToObj(index -> index + 1 + ". " + matches.get(index))
                .collect(Collectors.toList());
        collect.forEach((val) -> {
            stringBuilder.append(val + "\n");
        });
        String output = stringBuilder.toString();
        if (output != null && output.length() > 1)
            return output.substring(0, output.length() - 1);
        else
            return "";
    }

    public Match finishMatch(String matchTitle) {
        Match match = findMatchByMatchTitle(matchTitle);
        if (match != null) {
            match.setStatus(1);
        }
        return match;
    }

    private void removeDoneMatches() {
        Predicate<Match> condition = match -> match.getStatus() == 1;
        matches.removeIf(condition);
    }


}

