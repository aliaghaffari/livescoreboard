package models;

import java.util.Comparator;

public class MatchCompare implements Comparator<Match> {
    public int compare(Match match1, Match match2) {
        if (match1.getTotal_score() > match2.getTotal_score())
            return -1;
        else if (match1.getTotal_score() < match2.getTotal_score())
            return 1;
        else {
            int com = match2.getStartTime().compareTo(match1.getStartTime());
            return com;
        }
    }
}
