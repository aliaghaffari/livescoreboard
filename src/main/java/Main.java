import models.Match;
import models.ScoreBoard;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Match first_match = new Match("Mexico", "Canada");
        Match second_match = new Match("Spain", "Brazil");
        Match third_match = new Match("Germany", "France");
        Match fourth_match = new Match("Uruguay", "Italy");
        Match fifth_match = new Match("Argentina", "Australia");

        ScoreBoard.getInstance().startMatch(first_match);
        Thread.sleep(1000);
        ScoreBoard.getInstance().startMatch(second_match);
        Thread.sleep(1000);
        ScoreBoard.getInstance().startMatch(third_match);
        Thread.sleep(1000);
        ScoreBoard.getInstance().startMatch(fourth_match);
        Thread.sleep(1000);
        ScoreBoard.getInstance().startMatch(fifth_match);

        ScoreBoard.getInstance().updateMatchScore("Mexico-Canada", "0-5");
        ScoreBoard.getInstance().updateMatchScore("Spain-Brazil", "10-2");
        ScoreBoard.getInstance().updateMatchScore("Germany-France", "2-2");
        ScoreBoard.getInstance().updateMatchScore("Uruguay-Italy", "6-6");
        ScoreBoard.getInstance().updateMatchScore("Argentina-Australia", "3-1");
        System.out.println(ScoreBoard.getInstance().summary());
        System.out.println();
        System.out.println("-----After finish a match-----");
        ScoreBoard.getInstance().finishMatch("Argentina-Australia");
        System.out.println(ScoreBoard.getInstance().summary());
    }

}
