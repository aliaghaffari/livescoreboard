import models.Match;
import models.ScoreBoard;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ScoreBoardTest {
    ScoreBoard scoreBoard = ScoreBoard.getInstance();
    String homeTeam = "Mexico";
    String awayTeam = "Canada";

    @AfterEach
    void deleteAll(){
        scoreBoard.deleteAll();
    }


    @Test
    void startMatchTest() {
        /* When Call startMach method
         * The size of matches list in ScoreBoard must be add
         */

        //Init new match
        Match match = new Match(homeTeam, awayTeam);

        //call startMatch from ScoreBoard
        scoreBoard.startMatch(match);

        //We expect this match add to list.
        assertEquals(1, scoreBoard.matches.size());
    }


    @Test
    void matchTitleTest() {
        /* Check matchTile: matchTitle is a key for any match.
         * by matchTitle we can access to match in list.
         */

        //Init new match
        Match match = new Match(homeTeam, awayTeam);

        // When we start match we expect title for this match like be this : Mexico-Canada.
        String expectTitle = "Mexico-Canada";
        assertEquals(expectTitle, match.getMatchTitle());
    }

    @Test
    void updateMatchTotalScoreTest() {
        /* Test updateMatchScore method.
         * This method is for change result of match and calculate total score.
         *  We need total score for show result of all matches.
         * Becuase The order of display of matches is based on the total score and add to list order.
         */

        //Init new match
        Match match = new Match(homeTeam, awayTeam);

        //call startMatch from ScoreBoard
        scoreBoard.startMatch(match);

        // Call updateMatchScore from Match class
        match = scoreBoard.updateMatchScore("Mexico-Canada", "10-2");

        //We expect for this match total score be equal 12
        assertEquals(12, match.getTotal_score());
    }

    @Test
    void toStringMethodMatchTest() {
        /*
        * This test for showing latest match result after updateScore or end of a match.
        * I override toString method for Match class to show what I want.
        */

        //Init new match
        Match match = new Match(homeTeam, awayTeam);

        //call startMatch from ScoreBoard
        scoreBoard.startMatch(match);

        // Call updateMatchScore from Match class
        match = scoreBoard.updateMatchScore("Mexico-Canada", "10-2");

        // We expect show me this output : Mexico 10 - Canada 2
        String expect = "Mexico 10 - Canada 2";
        assertEquals(expect, match.toString());

    }

    @Test
    void summaryMatchesTest() {
        /*
         * by use toString method for Match class to show what I want.
         */

        /* We expect after call summary method can see this :
        * if we have one matche :
        * 1. Mexico 10 - Canada 2
        *
        * If we have two matches :
        * 1. Mexico 10 - Canada 2
        * 2. Spain 1 - Brazil 4
        */

        //Init new match
        Match first_match = new Match(homeTeam, awayTeam);
        Match second_match = new Match("Spain", "Brazil");

        //call startMatch from ScoreBoard
        scoreBoard.startMatch(first_match);
        scoreBoard.startMatch(second_match);

        // Call updateMatchScore from Match class
        scoreBoard.updateMatchScore("Mexico-Canada", "10-2");
        scoreBoard.updateMatchScore("Spain-Brazil", "8-5");

        // We expect show me this output : Mexico 10 - Canada 2
        String expect = "1. Spain 8 - Brazil 5\n" +
                        "2. Mexico 10 - Canada 2" ;
        assertEquals(expect, scoreBoard.summary());

    }

    @Test
    void finishMatchTest() {
         /*
          * When we register the end of a match,
          * We expect that we don't see it after call summary method.
          *
          * We use status field in Match class.
          * Default value for status is 0.
          * After call FinishMatch value of status is equal 1.
          * We add a other method to summary that remove all matches that value of status is equal 1.
          */

        //Init new match
        Match match = new Match(homeTeam, awayTeam);

        //call startMatch from ScoreBoard
        scoreBoard.startMatch(match);

        // Call finishMatch method
        match = scoreBoard.finishMatch("Mexico-Canada");

        // We expect value of status is equal 1.
        assertEquals(1, match.getStatus());

        /*
         * Also after call summary we expect don't see anything
         * We add removeDoneMatch and call it in summary method.
         */

        String expect = "";
        assertEquals(expect, scoreBoard.summary());
    }

}