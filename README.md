# Live Score Board


I try to write a simple code for this task.

First need to start a match by startMatch method. This method add a match to list.

# For example

create a instance of Match :

Match first_match = new Match("Mexico", "Canada");


And start Match by this :

ScoreBoard.getInstance().startMatch(first_match);


You can update score by giving match key to updateMatchScore method :

ScoreBoard.getInstance().updateMatchScore("Mexico-Canada", "0-5");

for show result of match you can call summary method in ScoreBoard. Like this :

System.out.println(ScoreBoard.getInstance().summary());

output :

1. Mexico 0 - Canada 5

----------------------------------

You can finish a match by giving match key to finishMatch method :

ScoreBoard.getInstance().finishMatch("Mexico-Canada");

----------------------------------


# All matches that are in PDF file :

Match first_match = new Match("Mexico", "Canada");

Match second_match = new Match("Spain", "Brazil");

Match third_match = new Match("Germany", "France");

Match fourth_match = new Match("Uruguay", "Italy");

Match fifth_match = new Match("Argentina", "Australia");


ScoreBoard.getInstance().startMatch(first_match);

Thread.sleep(1000);

ScoreBoard.getInstance().startMatch(second_match);

Thread.sleep(1000);

ScoreBoard.getInstance().startMatch(third_match);

Thread.sleep(1000);

ScoreBoard.getInstance().startMatch(fourth_match);

Thread.sleep(1000);

ScoreBoard.getInstance().startMatch(fifth_match);

ScoreBoard.getInstance().updateMatchScore("Mexico-Canada", "0-5");

ScoreBoard.getInstance().updateMatchScore("Spain-Brazil", "10-2");

ScoreBoard.getInstance().updateMatchScore("Germany-France", "2-2");

ScoreBoard.getInstance().updateMatchScore("Uruguay-Italy", "6-6");

ScoreBoard.getInstance().updateMatchScore("Argentina-Australia", "3-1");

System.out.println(ScoreBoard.getInstance().summary());

System.out.println();

System.out.println("-----After finish a match-----");

ScoreBoard.getInstance().finishMatch("Argentina-Australia");

System.out.println(ScoreBoard.getInstance().summary());

#output :

1. Uruguay 6 - Italy 6
2. Spain 10 - Brazil 2
3. Mexico 0 - Canada 5
4. Argentina 3 - Australia 1
5. Germany 2 - France 2

-----After finish a match-----
1. Uruguay 6 - Italy 6
2. Spain 10 - Brazil 2
3. Mexico 0 - Canada 5
4. Germany 2 - France 2
